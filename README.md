# Console Timestamper

Simply prepends [standard NodeJS console methods](https://nodejs.org/api/console.html) with a timestamp
formatted by [MomentJS](https://momentjs.com/docs/#/displaying/format/). There is no coverage of the
[inspector-only methods](https://nodejs.org/api/console.html#console_inspector_only_methods)

## Installation

```
npm i [--save, --save-dev, -g] console-timestamper
```

## Usage

```js
const Timestamper = require("console-timestamper");
const logger = new Timestamper("LLLL");
logger.log("Woo! Automagic timestamps in console");
```

Or

```ts
import Timestamper from "console-timestamper";
const logger = new Timestamper("LLLL");
logger.error("Wow, I now have any idea at all when this threw");
```

But for the love of God never

```js
const Timestamper = require("console-timestamper");
console = new Timestamper("LLLL");
console.log("WCGW if I pollute prototypes?");
```

## How it Looks

Running `npm test` gets you this.

```
[15:20:18.183]  console.assert()
[15:20:18.339]    Assertion failed: false assertion
[15:20:18.339]  console.count() and .countReset()
[15:20:18.339]    default: 1
[15:20:18.339]    default: 2
[15:20:18.339]    abc: 1
[15:20:18.339]    abc: 2
[15:20:18.339]    default: 3
[15:20:18.340]    resetting abc
[15:20:18.340]    abc: 1
[15:20:18.340]  console.debug(), .info(), and .log()
[15:20:18.340]    debug with 1 numeric substitution
[15:20:18.341]    plain ol' log
[15:20:18.341]    info with 2 substitutions
[15:20:18.341]  console.dir()
[15:20:18.341]
{ [Number: 1530822018341]
  _isAMomentObject: true,
  _isUTC: false,
  _pf: [Object],
  _locale: [Locale],
  _d: 2018-07-05T20:20:18.341Z,
  _isValid: true }
[15:20:18.345]
[ { a: 1, b: 'Y' }, { a: 'Z', b: 2 } ]
[15:20:18.346]  console.group(), .groupCollapsed(), and .groupEnd()
[15:20:18.346]    That's how I've been doing it
[15:20:18.346]  console.error() and .warn()
[15:20:18.346]    Err ma gerd
[15:20:18.347]    !WARNING!
[15:20:18.347]  Tables
[15:20:18.347]
┌─────────┬─────┬─────┐
│ (index) │  a  │  b  │
├─────────┼─────┼─────┤
│    0    │  1  │ 'Y' │
│    1    │ 'Z' │  2  │
└─────────┴─────┴─────┘
[15:20:18.350]  Trace: console.trace()
    at Timestamper.trace (/mnt/u/Users/mdoug/Git/timestamper/index.js:150:17)
    at Object.<anonymous> (/mnt/u/Users/mdoug/Git/timestamper/test.js:39:8)
    at Module._compile (internal/modules/cjs/loader.js:689:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:700:10)
    at Module.load (internal/modules/cjs/loader.js:599:32)
    at tryModuleLoad (internal/modules/cjs/loader.js:538:12)
    at Function.Module._load (internal/modules/cjs/loader.js:530:3)
    at Function.Module.runMain (internal/modules/cjs/loader.js:742:12)
    at startup (internal/bootstrap/node.js:236:19)
    at bootstrapNodeJSCore (internal/bootstrap/node.js:560:3)
[15:20:18.350]  console.time() and .timeEnd()
[15:20:18.351]    Test duration: 12.308ms
```