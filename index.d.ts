// Copyright 2018 Mike Doughty <mike@saverio.design>

/** Prepends standard global console methods with a user-defined timestamp */
export default class Timestamper {
    /** See [MomentJS](https://momentjs.com/docs/#/displaying/format/) for valid argument strings */
    constructor (format: string);
    /**
     * A simple assertion test that verifies whether `value` is truthy.
     * If it is not, an `AssertionError` is thrown. If provided, the error
     * `message` is formatted using `util.format()` and used as the error
     * message.
     */
    public assert (condition?: boolean, message?: any, ...data: any[]): void;
    /** 
     * When `stdout` is a TTY, calling `Timestamper.clear()` will attempt
     * to clear the TTY. When `stdout` is not a TTY, this method does nothin
     */
    public clear (): void;
    /**
     * Maintains an internal counter specific to `label` and outputs to `stdout`
     * the number of times `Timestamper.count()` has been called with the given
     * `label`.
     */
    public count (label?: string): void;
    /** Resets the internal counter specific to `label`. */
    public countReset (label?: string): void;
    /** The `Timestamper.debug()` function is an alias for `Timestamper.log()`. */
    public debug (message?: any, ...optionalParams: any[]): void;
    /**
     * Uses `util.inspect()` on `obj` and prints the resulting string to `stdout`.
     * This function bypasses any custom `inspect()` function defined on `obj`.
     */
    public dir (value?: any, ...optionalParams: any[]): void;
    /**
     * This method calls `Timestamper.log()` passing it the arguments received.
     * Please note that this method does not produce any XML formatting
     */
    public dirxml (value?: any, ...optionalParams: any[]): void;
    /** Prints to `stderr` witn newline. */
    public error (message?: any, ...optionalParams: any[]): void;
    /**
     * Increases indentation of subsequent lines by two spaces.
     * If one or more `label`s are provided, those are printed first
     * without the additional indentation.
     */
    public group (...label: any[]): void;
    /** The `Timestamper.groupCollapsed()` function is an alias for `Timestamper.group()`. */
    public groupCollapsed (...label: any[]): void;
    /** Decreases indentation of subsequent lines by two spaces. */
    public groupEnd (): void;
    /** The `Timestamper.info()` function is an alias for `Timestamper.log()`. */
    public info (message?: any, ...optionalParams: any[]): void;
    /** Prints to `stdout` with newline. */
    public log (message?: any, ...optionalParams: any[]): void;
    /**
     * This method does not display anything unless used in the inspector.
     * Prints to `stdout` the array `array` formatted as a table.
     */
    public table (...tabularData: any[]): void;
    /**
     * Starts a timer that can be used to compute the duration of an operation.
     * Timers are identified by a unique `label`.
     */
    public time (label?: string): void;
    /**
     * Stops a timer that was previously started by calling `Timestamper.time()` and
     * prints the result to `stdout`.
     */
    public timeEnd (label?: string): void;
    /**
     * Prints to `stderr` the string 'Trace :', followed by the `util.format()`
     * formatted message and stack trace to the current position in the code.
     */
    public trace (message?: any, ...optionalParams: any[]): void;
    /** The `Timestamper.warn()` function is an alias for `Timestamper.error()`. */
    public warn (message?: any, ...optionalParams: any[]): void;
}
