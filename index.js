// Copyright 2018 Mike Doughty <mike@saverio.design>

const moment = require("moment");

/** Prepends standard global console methods with a user-defined timestamp */
class Timestamper {

    /** See [MomentJS](https://momentjs.com/docs/#/displaying/format/) for valid argument strings */
    constructor (format) {
        if (typeof(format) !== "string") throw new Error("format must be a string");
        this.format = format;
    }

    /** Returns a timestamp as defined by the user */
    ts () {
        return `[${moment().format(this.format)}] `;
    }

    /**
     * A simple assertion test that verifies whether `value` is truthy.
     * If it is not, an `AssertionError` is thrown. If provided, the error
     * `message` is formatted using `util.format()` and used as the error
     * message.
     */
    assert (condition, message, ...data) {
        if (!condition) process.stdout.write(this.ts());
        console.assert(condition, message, ...data);
    }

    /** 
     * When `stdout` is a TTY, calling `Timestamper.clear()` will attempt
     * to clear the TTY. When `stdout` is not a TTY, this method does nothin
     */
    clear () {
        console.clear();
    }    

    /**
     * Maintains an internal counter specific to `label` and outputs to `stdout`
     * the number of times `Timestamper.count()` has been called with the given
     * `label`.
     */
    count (label) {
        process.stdout.write(this.ts());
        console.count(label);
    }

    /** Resets the internal counter specific to `label`. */
    countReset (label) {
        console.countReset(label);
    }

    /** The `Timestamper.debug()` function is an alias for `Timestamper.log()`. */
    debug (message, ...optionalParams) {
        this.log(message, ...optionalParams);
    }

    /**
     * Uses `util.inspect()` on `obj` and prints the resulting string to `stdout`.
     * This function bypasses any custom `inspect()` function defined on `obj`.
     */
    dir (value, ...optionalParams) {
        process.stdout.write(this.ts() + '\n');
        console.dir(value, ...optionalParams);
    }

    /**
     * This method calls `Timestamper.log()` passing it the arguments received.
     * Please note that this method does not produce any XML formatting
     */
    dirxml (value, ...optionalParams) {
        process.stdout.write(this.ts() + '\n');
        console.dirxml(value, ...optionalParams);
    }

    /** Prints to `stderr` witn newline. */
    error (message, ...optionalParams) {
        if (message !== undefined) {
            process.stderr.write(this.ts());
            console.error(message, ...optionalParams);
        } else console.error();
    }

    /**
     * Increases indentation of subsequent lines by two spaces.
     * If one or more `label`s are provided, those are printed first
     * without the additional indentation.
     */
    group (...label) {
        if (label.length) process.stdout.write(this.ts());
        console.group(...label);
    }

    /** The `Timestamper.groupCollapsed()` function is an alias for `Timestamper.group()`. */
    groupCollapsed (...label) {
        this.group(...label);
    }

    /** Decreases indentation of subsequent lines by two spaces. */
    groupEnd () {
        console.groupEnd();
    }

    /** The `Timestamper.info()` function is an alias for `Timestamper.log()`. */
    info (message, ...optionalParams) {
        this.log(message, ...optionalParams);
    }

    /** Prints to `stdout` with newline. */
    log (message, ...optionalParams) {
        if (message !== undefined) {
            process.stdout.write(this.ts());
            console.log(message, ...optionalParams);
        }
        else console.log();
    }

    /**
     * This method does not display anything unless used in the inspector.
     * Prints to `stdout` the array `array` formatted as a table.
     */
    table (...tabularData) {
        process.stdout.write(this.ts() + "\n");
        console.table(...tabularData);
    }

    /**
     * Starts a timer that can be used to compute the duration of an operation.
     * Timers are identified by a unique `label`.
     */
    time (label) {
        console.time(label);
    }

    /**
     * Stops a timer that was previously started by calling `Timestamper.time()` and
     * prints the result to `stdout`.
     */
    timeEnd (label) {
        process.stdout.write(this.ts());
        console.timeEnd(label);
    }

    /**
     * Prints to `stderr` the string 'Trace :', followed by the `util.format()`
     * formatted message and stack trace to the current position in the code.
     */
    trace (message, ...optionalParams) {
        process.stdout.write(this.ts());
        console.trace(message, ...optionalParams);
    }

    /** The `Timestamper.warn()` function is an alias for `Timestamper.error()`. */
    warn (message, ...optionalParams) {
        this.error(message, ...optionalParams);
    }

}

module.exports = Timestamper;
module.exports.default = module.exports;
