// Copyright 2018 Mike Doughty <mike@saverio.design>

import Timestamper from './index';
import * as moment from 'moment';

let logger = new Timestamper('HH:mm:ss.SSS');

logger.time("Test duration");
logger.group("Timestamper.assert()");
logger.assert(false, "false assertion");
logger.assert(true, "true assertion");
logger.groupEnd();

logger.group("Timestamper.count() and .countReset()");
logger.count();
logger.count();
logger.count('abc');
logger.count('abc');
logger.count();
logger.log("resetting abc");
logger.countReset("abc");
logger.count("abc");
logger.groupEnd();

logger.group("Timestamper.debug(), .info(), and .log()");
logger.debug("debug with %d numeric substitution", 1);
logger.log("plain ol' log");
logger.info("info with %d %s", 2, "substitutions");
logger.groupEnd();

logger.log("Timestamper.dir()");
logger.dir(moment(), {depth: 0});
logger.dirxml([{ a: 1, b: 'Y' }, { a: 'Z', b: 2 }]);

logger.group("Timestamper.group(), .groupCollapsed(), and .groupEnd()");
logger.log("That's how I've been doing it");
logger.groupEnd();

logger.group("Timestamper.error() and .warn()");
logger.error("Err ma gerd");
logger.warn("!WARNING!");
logger.groupEnd();

logger.log("Tables");
logger.table([{ a: 1, b: 'Y' }, { a: 'Z', b: 2 }]);

logger.trace("Timestamper.trace()");

logger.group("Timestamper.time() and .timeEnd()");
logger.timeEnd("Test duration");
logger.groupEnd();